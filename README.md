# Projet 1 - LP CGIR

![Schéma de l'architecture](media/architectureProjet1.jpg)

## Répartition des tâches :

- Hugo : Réseau
- Gael : DNS & DHCP
- Theo : Web & Mail

## Checklist du travail à réaliser

### Travail obligatoire

- [x] Réseau partie publique / privée (NAT + DMZ)
- [x] Serveur web acccessible aux autres groupes
- [x] Echange de mail d'un groupe à l'autre
- [x] Partie privée sécurisée par un pc sous Linux (firewall pfSense)
- [x] Routage intervlan.
- [x] DHCP + DYNDNS.

### Bonus (toujours sur la feuillle du projet) :

- [x] IPv4 + IPv6.
- [ ] VPN pour pouvoir accéder au réseau depuis les réseaux des autres groupes
- [x] Réseau Wifi autorisant uniquement les flux HTTP

## Plan d'adressage

### IPV4

| VLAN | Nom          | Adresse Réseau | CIDR | Masque de Sous-Réseau | Passerelle    | Broadcast     |
|------|--------------|----------------|------|-----------------------|---------------|---------------|
| X    | DMZ          | 10.28.0.0      | /28  | 255.255.255.240       | 10.28.0.14    | 10.28.0.15    |
| X    | WAN          | 172.16.48.0    | /30  | 255.255.255.252       | 172.16.48.2   | 172.16.48.3   |
| 10   | Comptabilité | 192.168.1.0    | /26  | 255.255.255.192       | 192.168.1.62  | 192.168.1.63  |
| 11   | WIFI         | 192.168.1.176  | /28  | 255.255.255.240       | 192.168.1.190 | 192.168.1.191 |
| 12   | Production   | 192.168.1.64   | /26  | 255.255.255.192       | 192.168.1.126 | 192.168.1.127 |
| 13   | Management   | 192.168.1.160  | /28  | 255.255.255.240       | 192.168.1.174 | 192.168.1.175 |
| 14   | Serveurs     | 192.168.1.128  | /27  | 255.255.255.224       | 192.168.1.158 | 192.168.1.159 |

### IPv6

| VLAN | Nom          | Adresse Réseau     | CIDR | Passerelle                           | Broadcast                            |
|------|--------------|--------------------|------|--------------------------------------|--------------------------------------|
| X    | DMZ          | 2001:0:abcd:1300:: | /64  | 2001:0:abcd:1300:ffff:ffff:ffff:fffe | 2001:0:abcd:1300:ffff:ffff:ffff:ffff |
| X    | WAN          | 2001:0:abcd:1200:: | /64  | 2001:0:abcd:1200::2                  | 2001:0:abcd:1200:ffff:ffff:ffff:ffff |
| 10   | Comptabilité | 2001:0:abcd::      | /64  | 2001:0:abcd::ffff:ffff:ffff:fffe     | 2001:0:abcd::ffff:ffff:ffff:ffff     |
| 11   | WIFI         | 2001:0:abcd:200::  | /64  | 2001:0:abcd::1                       | 2001:0:abcd:200:ffff:ffff:ffff:ffff  |
| 12   | Production   | 2001:0:abcd:100::  | /64  | 2001:0:abcd:100:ffff:ffff:ffff:fffe  | 2001:0:abcd:100:ffff:ffff:ffff:ffff  |
| 13   | Management   | 2001:0:abcd:1000:: | /64  | 2001:0:abcd:1000:ffff:ffff:ffff:fffe | 2001:0:abcd:1000:ffff:ffff:ffff:ffff |
| 14   | Serveurs     | 2001:0:abcd:1100:: | /64  | 2001:0:abcd:1100:ffff:ffff:ffff:fffe | 2001:0:abcd:1100:ffff:ffff:ffff:ffff |

## Plan de nommage

|       Nom       |   Service  |      IPv4     | IPv6                |
|:---------------:|:----------:|:-------------:|---------------------|
| ns.nvt.org      | DHCP & DNS | 192.168.1.145 | 2001:0:abcd:1100::1 |
| s1.nvt.org      | Switch     | 192.168.1.161 | 2001:0:abcd:1000::1 |
| smtp.nvt.org    | SMTP       | 10.28.0.1     | 2001:0:abcd:1300::1 |
| web.nvt.org     | WEB        | 10.28.0.1     | 2001:0:abcd:1300::1 |
